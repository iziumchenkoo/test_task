And(/^I select (.*) in (.*) dropdown$/) do |param, number|
  on(DropdownPage).choose_dropdown(number)
  on(DropdownPage).select_option(param)
end

And(/^(.*) displayed in (.*) dropdown$/) do |param, number|
  on(DropdownPage).check_selected_item(param, number)
end

Given(/^I visited dropdown page$/) do
  visit(DropdownPage)
end
