Given(/^I visited checkbox page$/) do
  visit(CheckboxPage)
end

When(/^I change checkbox status$/) do
  on(CheckboxPage).select_checkbox
end

Then(/^Checkbox status is changed$/) do
  on(CheckboxPage).check_selected_checkbox
end

When(/^I change radio button status on "(.*)"$/) do |param|
  on(CheckboxPage).select_radio_button(param)
end

Then(/^Radio button status is "(.*)"$/) do |param|
  on(CheckboxPage).check_selected_radio(param)
end

When(/^I change slider status$/) do
  on(CheckboxPage).select_slider
end

Then(/^Slider status is changed$/) do
  on(CheckboxPage).check_selected_slider
end

When(/^I change toggle status$/) do
  on(CheckboxPage).select_toggle
end

Then(/^Toggle status is changed$/) do
  on(CheckboxPage).check_selected_toggle
end