Given(/^I visited table page$/) do
  visit(TablePage)
end

Then(/^Jimmy's status is "(.*)"$/) do |status|
  on(TablePage).jim_status(status)
end

And(/^There are (\d+) rows with status "([^"]*)" and notes "([^"]*)"$/) do |num, status, notes|
  on(TablePage).status_notes(num, status, notes)
end

Then(/^Jimmy and Jamie rows have "([^"]*)"$/) do |warn|
  on(TablePage).warn_table(warn)
end