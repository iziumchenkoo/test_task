Feature: Checkbox 1

  Background:
    Given I visited checkbox page

  Scenario: Checkbox
    When I change checkbox status
    Then Checkbox status is changed

  Scenario: Radio button
    When I change radio button status on "Twice a day"
    Then Radio button status is "Twice a day"

  Scenario: Slider
    When I change slider status
    Then Slider status is changed

  Scenario: Toggle
    When I change toggle status
    Then Toggle status is changed