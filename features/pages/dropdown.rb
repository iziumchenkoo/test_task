class DropdownPage
  include PageObject
  require 'rspec'
  include RSpec::Matchers

  page_url 'https://semantic-ui.com/modules/dropdown.html'
  element(:first_dropdown, :css => '#selection ~ .dropdown')
  element(:second_dropdown, :css => '.another.dropdown .selection')
  element(:third_dropdown, :css => '.another.dropdown .fluid.selection')
  elements(:selection, :css => '.menu.transition.visible .item')
  element(:first_dropdown_text, :css => '#selection ~ .dropdown .text')
  element(:second_dropdown_text, :css => '.another.dropdown .selection .text')
  element(:third_dropdown_text, :css => '.another.dropdown .fluid.selection .text')


  def choose_dropdown (number)
    wait_until{first_dropdown_element.present?}
    if number == "first"
      first_dropdown_element.click
    elsif number == "second"
      second_dropdown_element.click
    else
      third_dropdown_element.click
    end
  end

  def select_option (param)
    wait_until(selection_elements[0].visible?)
    index = selection_elements.map(&:text).index(param)
    selection_elements[index].click
  end


  def check_selected_item (param, number)
    if number == "first"
      expect(first_dropdown_text_element.text).to eql(param)
    elsif number == "second"
      expect(second_dropdown_text_element.text).to eql(param)
    else
      expect(third_dropdown_text_element.text.include?(param)).to eql(true)
    end
  end
end