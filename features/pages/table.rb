class TablePage
  include PageObject
  require 'rspec'
  include RSpec::Matchers

  page_url 'https://semantic-ui.com/collections/table.html'

  def jim_status (status)
    (1..4).each do |index|
      temp = @browser.tr(:css => '#positive--negative ~ table>tbody>tr:nth-child('+index.to_s+')').td(:index => 0).text
      if temp.include?("Jimmy")
      @status = @browser.tr(:css => '#positive--negative ~ table>tbody>tr:nth-child('+index.to_s+')').td(:index => 1).text
      end
    end
    expect(@status).to eql(status)
  end

  def status_notes (num,status,notes)
    @count = 0
    (1..4).each do |index|
      temp_status = @browser.tr(:css => '#positive--negative ~ table>tbody>tr:nth-child('+index.to_s+')').td(:index => 1).text
      if temp_status.include?(status)
        @count +=1 if @browser.tr(:css => '#positive--negative ~ table>tbody>tr:nth-child('+index.to_s+')').td(:index => 2).text.include?(notes)
      end
    end
    expect(@count).to eql(num)
  end

  def warn_table(warn)
    @browser.tr(:css => '#warning ~ table>tbody .warning').td(:index => 0).text.include?("Jimmy")
    @browser.td(:css => '#warning ~ table>tbody .warning').parent.text.include?("Jamie")
  end
end