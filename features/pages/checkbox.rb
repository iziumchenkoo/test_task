class CheckboxPage
  include PageObject
  require 'rspec'
  include RSpec::Matchers

  page_url 'https://semantic-ui.com/modules/checkbox.html'
  element(:checkbox, :css => '#checkbox ~ div')
  elements(:radio_button, :css => '.another.example .field .radio')
  element(:slider, :css => '#slider ~ div')
  element(:toggle, :css => '#toggle ~ div')
  element(:selected_radio, :css => '.another.example .field .radio.checkbox.checked')
  element(:checkbox_selected, :css => '#checkbox ~ .ui.checkbox.checked')
  element(:slider_selected, :css => '#slider ~ .ui.checkbox.checked')
  element(:toggle_selected, :css => '#toggle ~ .ui.checkbox.checked')


  def select_checkbox
    wait_until{checkbox_element.visible?}
    checkbox_element.click
  end

  def select_radio_button (param)
    wait_until{radio_button_elements[0].visible?}
    index = radio_button_elements.map(&:text).index(param)
    radio_button_elements[index].click
  end

  def select_slider
    wait_until{slider_element.visible?}
    slider_element.click
  end

  def select_toggle
    wait_until{toggle_element.visible?}
    toggle_element.click
  end

  def check_selected_slider
    expect(slider_selected_element.visible?).to eql(true)
  end

  def check_selected_toggle
    expect(toggle_selected_element.visible?).to eql(true)
  end

  def check_selected_checkbox
    expect(checkbox_selected_element.visible?).to eql(true)
  end

  def check_selected_radio (param)
    expect(selected_radio_element.text).to eql(param)
  end
end