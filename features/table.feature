Feature: Table

      Scenario: Positive / Negative table
        When I visited table page
        Then Jimmy's status is "Approved"
        And There are 2 rows with status "Approved" and notes "None"

     Scenario: Warning table
       When I visited table page
       Then Jimmy and Jamie rows have "!"