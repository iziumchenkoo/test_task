Feature: Dropdown

  Background:
    Given I visited dropdown page

  Scenario: Selection
    When I select Female in first dropdown
    And I select Male in second dropdown
    And I select Christian in third dropdown
    Then Female displayed in first dropdown
    And Male displayed in second dropdown
    And Christian displayed in third dropdown